# nineball

> zer0the0ry's nineball bash script

----

## The Magic Nineball

For thousands of years the mystics held the secret of the magic nineball. Upon an Antarctic expedition a small team of archaeologists happened upon it. Using this magic, they have since transcended time and space. This power is now in your hands.

## How to play

Think long and hard about a question or the magic won't work. Run the script and type in your question. Like magic your answer will appear.


