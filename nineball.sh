#!/bin/bash

# zero's nine ball

printf "\n"
printf "\033[38;2;0;0;255m         ####\033[38;2;0;255;0m The Doomed Nine Ball of Doom\033[38;2;0;0;255m #### \033[0m\n "
printf "\n"
read -p "What is your well thought out highly intelligent question? " question

# Making sure a question was damn well asked
if [ -z "$question" ]
then
	echo "I SAID ASK A QUESTION!"
	exit 1
fi

# Nine random numbers for the nine ball
magic=$((RANDOM%9))
printf "\n"

# This is where the magic happens
case $magic in
	"0") echo "Nein!";;
	"1") echo "Nein Nein!";;
	"2") echo "Nein Nein Nein!";;
	"3") echo "Search deep within yourself. You already know the answer.";;
	"4") echo "There is no spoon.";;
	"5") echo "Ask again in exactly 42 seconds.";;
	"6") echo "Fine. YES! Are you happy now?";;
	"7") echo "Forget your question, You're going to be rich!";;
	"8") echo "It doesn't matter. The world ends next week.";;
esac
printf "\n"
exit 0
